{
  description = "Centaurea's project to bootstrap the infrastructure";

  inputs.devshell.url = "github:numtide/devshell";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, flake-utils, devshell, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
    let pkgs = import nixpkgs {
      inherit system;
        overlays = [ devshell.overlay ];
      };
    in
    {
      devShell =
        pkgs.devshell.mkShell {
          imports = [ (pkgs.devshell.importTOML ./devshell.toml) ];
          packages = import ./packages.nix { inherit pkgs; };
        };
    });
}
